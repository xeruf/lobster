module Main where

import           Lobster.Algorithm
import           Lobster.IO

import           Control.Monad.State
import           System.Random

main :: IO ()
main = do
  teams <- readTeams
  games <- readGames
  -- evalState (generateRandomMatchups teams games) <$> newStdGen >>= print
  print $ generateRound teams games

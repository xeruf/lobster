module Lobster.Types where

type Team = String
type Matchup = (Team, Team)

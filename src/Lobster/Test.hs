module Lobster.Test
  ( main
  )
where

import           Lobster.Algorithm
import           Lobster.IO

import           Control.Monad.State
import           Data.Map.Strict                ( fromList )
import           Data.Ratio
import           System.Random

import           Test.Hspec
import           Test.QuickCheck.Exception      ( evaluate )

tests = do
  describe "selectRandom" $ do
    it "is reliable"
      $   evalState
            (do
              r1 <- selectRandom [('a', 1 :: Int)]
              r2 <- selectRandom [('a', 3 :: Int), ('b', 3)]
              return (r1, r2)
            )
      <$> newStdGen
      >>= (`shouldBe` ('a', 'a'))
    it "is weighted" $ do
      let sGen = mkStdGen 3
      countGen 'b' 80 [('a', 1), ('b', 3)] sGen `shouldBe` 52
      countGen 'b' 80 [('a', 1), ('b', 4)] sGen `shouldBe` 61
      countGen 'b' 99 [('a', 0), ('b', 0.1)] sGen `shouldBe` 99
  describe "matchup list" $ do
    let matchups = [("T1", "T2"), ("T3", "T1"), ("T2", "T1")]
        teams    = uniqueTeams matchups
    it "can generate weights" $ do
      let expectedWeights = fromList [("T2", 1 % 81), ("T3", 1 % 9)]
      generateWeights (filter (/= "T1") teams) (opponents "T1" matchups)
        `shouldBe` expectedWeights
      let accumulatedWeights = [("T3", 1 % 9), ("T2", 10 % 81)]
      accumulateWeights expectedWeights `shouldBe` accumulatedWeights
      selectAt (9 % 81) accumulatedWeights `shouldBe` "T3"
      selectAt (10 % 81) accumulatedWeights `shouldBe` "T2"
      evaluate (selectAt 1 accumulatedWeights) `shouldThrow` anyException
    it "can be turned into a matchupMap" $ do
      map gameTuple ["T1 vs T2", "T3 vs T1", "T2 vs T1"] `shouldBe` matchups
      filterInvolved "T3" matchups `shouldBe` [("T3", "T1")]
      filterInvolved "T1" matchups `shouldBe` matchups
      matchupMap ["T1", "T2"] matchups
        `shouldBe` fromList [("T1", [("T2", 2)]), ("T2", [("T1", 2)])]
    it "can generate matchups" $ do
      evalState (generateRandomMatchups teams matchups) (mkStdGen 3)
        `shouldBe` [("T1", "T3"), ("T2", "--")]
      --countGen ["T1 vs T2", "T3 vs --"] 10 (generateRandomMatchups teams matchups) (mkStdGen 3) `shouldBe` []
    it "calculates weights from chronology" $ do
      calculateHistoryWeights ["1", "2"] (zip ["1", "2", "1"] [1 ..])
        `shouldBe` fromList [("1", 4), ("2", 2)]
      let expectedWeights = fromList [("T2", 4), ("T3", 2)]
      calculateHistoryWeights
          (filter (/= "T1") teams)
          (numberedOpponents "T1" $ numberedHistory teams matchups)
        `shouldBe` expectedWeights
    it "creates a numbered history" $ do
      numberedHistory ["T"] [] `shouldBe` [(("T", "--"), 1)]
      numberedHistory teams [] `shouldBe` zip (zip teams $ repeat "--") [1 ..]
      numberedHistory teams [head matchups]
        `shouldBe` [(head matchups, 1), (("T3", "--"), 2)]
    let bestRound = [("T1", "--"), ("T2", "T3")]
    it "judges rounds" $ do
      head (ratedRounds teams matchups) `shouldBe` (0, bestRound)
      generateRound teams matchups `shouldBe` bestRound
    let moreMatches = matchups ++ [("T3", "--"), ("T3", "T2")]
    it "properly rates best round" $ do
      head (ratedRounds teams moreMatches) `shouldBe` (3, bestRound)
      generateRound teams (moreMatches ++ [("T1", "--")])
        `shouldBe` [("T1", "T3"), ("T2", "--")]
    it "starts every team with a break" $ do
      ratedRounds ["Team"] [] `shouldBe` [(1, [("Team", "--")])]

main :: IO ()
main = hspec $ do
  tests

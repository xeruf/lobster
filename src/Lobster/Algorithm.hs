{-# LANGUAGE TupleSections #-}
{-#LANGUAGE ScopedTypeVariables, TypeApplications #-}

module Lobster.Algorithm where

import           Lobster.Types

import           Control.Monad.State
import           Data.Bifunctor
import           Data.Function                  ( (&) )
import           Data.List                      ( sortOn
                                                , sort
                                                , group
                                                , find
                                                )
import           Data.Map.Strict                ( Map
                                                , fromList
                                                , toList
                                                , adjust
                                                )
import           Data.Maybe                     ( fromJust )
import           Data.Ratio
import           System.Random

-- | Filter the matchups list to only include matchups that involve the given Team.
filterInvolved :: Team -> [Matchup] -> [Matchup]
filterInvolved team = filter (\(t1, t2) -> t1 == team || t2 == team)

-- | Return the team in the match which is not the given one.
-- | It is an error to pass a Matchup that does not involve the team.
otherTeam :: Team -> Matchup -> Team
otherTeam t (t1, t2) | t1 == t = t2
                     | t2 == t = t1

-- | Return an ordered list of the Teams a Team played against from the matchup history.
opponents :: Team -> [Matchup] -> [Team]
opponents team = map (otherTeam team) . filterInvolved team

-- UNUSED

-- | Create a table of how many times every team faced every other.
matchupMap :: [Team] -> [Matchup] -> Map Team [(Team, Int)]
matchupMap teams games = fromList $ map
  (\team -> (team, opponents team games & filter (`elem` teams) & counts))
  teams
  where counts = map (\l -> (head l, length l)) . group . sort

-- ALGORITHM 1 - based on randomness

-- | Take a list of all opposing teams and previous encounters and
-- | generate probabilities for further matchups.
generateWeights :: [Team] -> [Team] -> Map Team (Ratio Int)
generateWeights = foldr (adjust (/ 9)) . fromList . flip zip [1, 1 ..]

accumulateWeights :: Map Team (Ratio Int) -> [(Team, Ratio Int)]
accumulateWeights =
  foldr
      (\item list ->
        list ++ [second (if null list then id else (+ (snd $ head list))) item]
      )
      []
    . toList

-- | Take a random generator and a list of tuples containing
-- | accumulated weights and values.
-- | Return a weighted random value of that list.
selectRandom :: (RandomGen g, Random r, Ord r, Num r) => [(a, r)] -> State g a
selectRandom dist = do
  gen <- get
  let (n, gen') = randomR (0, snd $ last dist) gen
  put gen'
  return . fst . fromJust $ find ((>= n) . snd) dist

selectAt :: (Ord r) => r -> [(a, r)] -> a
selectAt pos = fst . fromJust . find ((>= pos) . snd)

chooseOpponent :: (RandomGen g) => Team -> [Team] -> [Matchup] -> State g Team
chooseOpponent team teams =
  selectRandom . accumulateWeights . generateWeights teams . opponents team

generateRandomMatchups
  :: (RandomGen g) => [Team] -> [Matchup] -> State g [Matchup]
generateRandomMatchups teams history = case teams of
  []           -> pure []
  team    : [] -> pure [(team, "--")]
  t1 : t2 : [] -> pure [(t1, t2)]
  team    : ts -> do
    opponent :: Team <- chooseOpponent team ts history
    matchups         <- generateRandomMatchups (filter (/= opponent) ts) history
    return $ (team, opponent) : matchups

-- ALGORITHM 2 - abbreviated fully deterministic alpha-beta-search

type NumberedHistory = [(Matchup, Int)]
-- | Create a numbered history that includes each team at least once.
numberedHistory :: [Team] -> [Matchup] -> NumberedHistory
numberedHistory teams games = zip
  (  games
  ++ (teams & filter (\team -> null $ filterInvolved team games) & map
       (, "--")
     )
  )
  [1 ..]

numberedOpponents team games =
  zip (opponents team $ map fst games) (map snd games)

-- | Take a list of all teams and encounters (oldest first).
-- | Map each team to the frecency of encounters of that Team.
calculateHistoryWeights :: [Team] -> [(Team, Int)] -> Map Team Int
calculateHistoryWeights teams encounters = foldr
  (\(team, int) -> adjust (+ int) team)
  (fromList $ zip teams [0, 0 ..])
  encounters

-- | Take a list of teams and the matchup history.
-- | Return a list of possible rounds with ratings - the lower, the better.
evaluateMatchups :: [Team] -> NumberedHistory -> [(Int, [Matchup])]
evaluateMatchups [] _ = [(0, [])]
evaluateMatchups (team : ts) history =
  [ (snd x + fst m, (team, fst x) : (snd m))
  | x <- toList $ calculateHistoryWeights ts (numberedOpponents team history)
  , m <- evaluateMatchups (filter (/= (fst x)) ts) history
  ]

ratedRounds teams history =
  evaluateMatchups
      (if length teams `rem` 2 == 1 then teams ++ ["--"] else teams)
    $ numberedHistory teams history

-- | Take a list of teams and the matchup history.
-- | Returns a good round.
generateRound :: [Team] -> [Matchup] -> [Matchup]
generateRound teams = snd . head . sortOn fst . ratedRounds teams

-- Utilities for random generation

-- | Run a random generator a number of times and count the occurences of a specific result.
countGen :: (RandomGen r, Eq a) => a -> Int -> [(a, Float)] -> r -> Int
countGen _ 0 _ _ = 0
countGen c n dist gen =
  let (result, gen') = runState (selectRandom dist) gen
  in  countGen c (n - 1) dist gen' + (if result == c then 1 else 0)

instance (Integral r, Random r) => Random (Ratio r) where
  random gen =
    let (num, gen1) = random @r gen
        (den, gen2) = random @r gen1
    in  (num % den, gen2)
  randomR range gen =
    let n1        = numerator $ fst range
        n2        = numerator $ snd range
        d1        = denominator $ fst range
        d2        = denominator $ snd range
        d         = d1 * d2
        (n, gen') = randomR (n1 * d2, n2 * d1) gen
    in  (n % d, gen')

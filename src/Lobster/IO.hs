-- | Helpers for interacting with the filesystem.

module Lobster.IO where

import           Lobster.Types

import           Data.List
import           Data.List.Split

readLines :: FilePath -> IO [String]
readLines file = lines <$> readFile file
readGames :: IO [Matchup]
readGames = map gameTuple <$> readLines "games.txt"
readTeams :: IO [Team]
readTeams = readLines "teams.txt"

gameTuple :: String -> Matchup
gameTuple game = let t = splitOn " vs " game in (head t, last t)

uniqueTeams :: [Matchup] -> [Team]
uniqueTeams =
  map head . group . sort . foldr (\(t1, t2) list -> t1 : t2 : list) []

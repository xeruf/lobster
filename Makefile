.PHONY: all build run clean format

SRCDIR=src
OUTDIR=out

PROJECT_NAME=Lobster
MAIN_MODULE=Main
TEST_MODULE=${PROJECT_NAME}.Test

GHC_OPTIONS = -threaded -rtsopts -with-rtsopts=-N -odir ${OUTDIR} -hidir ${OUTDIR} -o ${OUTDIR}/main -i${SRCDIR}
GHC_OPTIONS+= -Wall -Wno-name-shadowing -Wno-unused-do-bind
#GHC_OPTIONS+= -Werror=incomplete-patterns

FIND_SOURCES=find ${SRCDIR} -type f -name '*.*hs'

all: clean test run

build:
	mkdir -p ${OUTDIR}
	ghc -O2 ${GHC_OPTIONS} ${MAIN_MODULE}

build-dev:
	mkdir -p ${OUTDIR}
	ghc ${GHC_OPTIONS} ${MAIN_MODULE}

build-test:
	mkdir -p ${OUTDIR}
	ghc ${GHC_OPTIONS} -o ${OUTDIR}/test ${TEST_MODULE} -main-is ${TEST_MODULE}.main

run: build
	${OUTDIR}/main

run-dev: build-dev build-test
	${OUTDIR}/test
	${OUTDIR}/main

run-watch:
	${FIND_SOURCES} | entr -dcr make run-dev

test: build-test
	${OUTDIR}/test

dev:
	find ${SRCDIR} -type f -name '*.*hs' | entr -dcr make test

clean:
	rm -rf ${OUTDIR}

format:
	find ${SRCDIR} -type f -name '*.*hs' -exec brittany --write-mode=inplace {} ';'
